package pw.hais.utils_demo.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.okhttp.Response;

import java.util.HashMap;
import java.util.Map;

import pw.hais.http.Http;
import pw.hais.http.base.Listener;
import pw.hais.http.utils.UtilConfig;
import pw.hais.utils_demo.R;

public class MainActivity extends Activity {
    private TextView text_hello;
    private ImageView image_test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text_hello = (TextView) findViewById(R.id.text_hello);
        image_test = (ImageView) findViewById(R.id.image_test);


        UtilConfig.init(getApplication().getApplicationContext());

        //普通Http-GET请求
        String url = "http://baidu.com";
        Map<String,String> data = new HashMap<>();
        Http.get(url, data, new Listener<String>() {
            @Override
            public void success(Response response, String result) {
                text_hello.setText("结果："+ result);
            }
        });


        //加载图片
        String image_url = "http://b.zol-img.com.cn/desk/bizhi/image/6/1920x1080/144064263261.jpg";
        Http.image(image_test, image_url, new Listener<Bitmap>() {
            @Override
            public void success(Response response, Bitmap result) {

            }
        });
    }


}

#OkHttp_Util

    OkHttp_Util 基于 正如它的名字一样，是基于 OkHttp 封装的一个 类库。
    另外本人根据自己的使用习惯，弄了个 Android快速开发工具库(主分支为Volley、OkHttp分支把网络框架换为了OkHttp)，欢迎各位指出不足。
    http://git.oschina.net/hais/Hais_Android_Utils/tree/OkHttp/Hais_Utils_Lib

    其次，OkHttp类库 封装之前，参考了大神的一些思路
    https://github.com/hongyangAndroid
    https://github.com/android-cjj
    还有 带我入门的 LiuZhi、LuoKun、Ban..。

#信息
    Git地址：http://git.oschina.net/hais
    博客地址： http://hais.pw

#功能
    1、普通的GET请求 get(String url, Map<String, String> params, Listener<?> listener);
    2、普通的POST请求 post(String url, Map<String, String> params, Listener<?> listener);
    3、直接POST Body  post(String url,String body,Listener<?> listener)
    4、文件上传 updateFile(String url, Map<String, String> params, File[] files, String[] fileKeys, Listener<?> listener);
    5、显示图片 displayImage(ImageView imageView, String url);
    6、缓存图片 cacheImage(String url);
    7、下载文件 download(String url, String fileDir, Listener<String> listener);
    8、取消请求 cancel(String url);

#说明
    1、Get、POST 请求，都可以根据自己的 需求 返回 String、JSONobject、Object。
    2、文件上传 没得测试 -.- ，没亮点。
    3、图片显示，增加 缓存模块，可以先缓存，后面断网后直接读取缓存中的显示， 显示淡出动画。
    4、下载文件，加了 个下载进度。

    注：所有 listener 的回掉，为了方便都 是在 主线程。
        listener.success 请求成功。
        listener.error 请求失败。
        listener.onString 请求返回的原始字符串。
        listener.onProgress 下载文件时的进度条
        listener.httpEnd 不管请求成功与否，都调用。

#使用方法
(1)普通的GET请求 (data可为null，listener的类型可以为 String、JSONObject或对象)
```java
String url = "http://baidu.com";
    Map<String,String> data = new HashMap<>();
    Http.get(url, data, new Listener<String>() {
        @Override
        public void success(Response response, String result) {
            text_hello.setText("结果："+ result);
        }
    });
 ```
 ------------------------------------------------------------------------------
 (2)普通的POST请求 (data必须有1个参数，data 也可为String、File、byte[]，listener的类型可以为 String、JSONObject或对象)
 ```java
 String url = "http://baidu.com";
     Map<String,String> data = new HashMap<>();
     Http.post(url, data, new Listener<String>() {
         @Override
         public void success(Response response, String result) {
             text_hello.setText("结果："+ result);
         }
     });
  ```
 ------------------------------------------------------------------------------
 (3)显示图片 (data必须有1个参数，data 也可为String、File、byte[])
 ```java
 String image_url = "http://b.zol-img.com.cn/desk/bizhi/image/6/1920x1080/144064263261.jpg";
 Http.image(image_view, image_url); //缓存并显示
 Http.image(image_url); //缓存
 //缓存，显示，并监听
 Http.image(image_test, image_url, new Listener<Bitmap>() {
            @Override
            public void success(Response response, Bitmap result) {

            }
        });
    }
  ```
 ------------------------------------------------------------------------------
 (4)其它使用方法
 ```java
    Http.download(String url, String fileDir, Listener<String> listener);   //下载1个文件
    Http.cancel(String url);    //根据URL取消本次请求
  ```